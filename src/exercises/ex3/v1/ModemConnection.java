package exercises.ex3.v1;

public interface ModemConnection {
     void dial(String pno);
     void hangup();
}
