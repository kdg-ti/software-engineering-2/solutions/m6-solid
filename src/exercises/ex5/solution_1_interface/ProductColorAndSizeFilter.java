package exercises.ex5.solution_1_interface;

import exercises.ex5.model.*;

import java.util.ArrayList;
import java.util.List;

public class ProductColorAndSizeFilter implements ProductFilter
{
    private ProductColor productColor;
	private ProductSize productSize;

    public ProductColorAndSizeFilter(ProductColor color,ProductSize size){
        productColor = productColor;
        productSize=size;
    }


	@Override
	public List<Product> filter(List<Product> products) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product:products)
		{
			if (product.getColor() == productColor && product.getSize() == productSize)
			{filteredProducts.add(product);}
		}
		return filteredProducts;	}
}
