package exercises.ex1;

public interface ISecurityService {

    public boolean checkAccess(User user);
}
