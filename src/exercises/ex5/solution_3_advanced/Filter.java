package exercises.ex5.solution_3_advanced;


import java.util.List;
import java.util.function.Predicate;

public class Filter<T> {
	public List<T> filter(List<T> items, Predicate<T> pt) {
		return items.stream().filter(pt).toList();
	}
}
