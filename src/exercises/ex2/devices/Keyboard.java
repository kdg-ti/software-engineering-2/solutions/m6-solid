package exercises.ex2.devices;

import exercises.ex2.core.Reader;

import java.util.Scanner;

public class Keyboard implements Reader {

	String heard;
	int counter;

	public Keyboard(String input){
		System.out.print(input + " is listening... ");
		Scanner mike = new Scanner(System.in);
		heard =mike.nextLine();
	}
    @Override
    public char read() {
        return heard.charAt(counter++);
    }

    @Override
    public boolean hasNext() {
        return counter < heard.length();
    }
}
