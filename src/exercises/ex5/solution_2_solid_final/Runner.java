package exercises.ex5.solution_2_solid_final;


import exercises.ex5.model.Product;
import exercises.ex5.model.ProductColor;
import exercises.ex5.model.ProductSize;

public class Runner {

  // This code builds on what we did in solution version 1
  // In version 1 filters have a lot of code in common
  // These filters only differ in the test the perform
  // The responsibility of the filters are
  //  - the filter algorithm
  //  - the test to perform
  // This solution separates these responsibilities (SRP)
  // It contains a concrete Filter class: ProductFilter, which contains the filter algoritme
  // its constructor gets the test to be executed as a parameter.
  // There is a ProductTest interface with concrete subclasses for each test to run.
  //
  // There is a second optimisation in this solution.
  // In version 1 there is a test on Color, a test on Size and a test on both.
  // Clearly the ColorAndSize test duplicates code that is present in the other two tests
  // We solve this problem using the composition pattern: We make a combined test (AND test) that takes the
  //  tests that must be combined as parameters in its constructor. The filter method of the combined tests,
  //  then combines the result of all these tests.
  //
  public static void main(String[] args) {
    ProductFilter pf = new ProductFilter();
    ProductTest ct = new ProductColorTest(ProductColor.RED);
    System.out.println("Red Products" + pf.filter(Product.getProducts(), ct));
    ProductTest st = new ProductSizeTest(ProductSize.XXL);
    System.out.println("XXL Products" + pf.filter(Product.getProducts(), st));
    ProductTest at = new ProductAndTest(ct, st);
    System.out.println("Red XXL Products" + pf.filter(Product.getProducts(), at));

  }
}
