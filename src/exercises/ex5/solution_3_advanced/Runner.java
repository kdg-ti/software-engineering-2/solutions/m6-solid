package exercises.ex5.solution_3_advanced;


import exercises.ex5.model.Product;
import exercises.ex5.model.ProductColor;
import exercises.ex5.model.ProductSize;
import java.util.function.Predicate;

public class Runner {

  // This builds further on the SOID solution and combines several techniques to build a more general solution
  // - generics: Filter works on any type
  // - lambda's for specifying tests
  // - Predicate (a functional interface returning a boolean), which also supports methods like and(otherPredicate) for combining predicates

  public static void main(String[] args) {
    Filter<Product> pf = new Filter<>();
    Predicate<Product> productColorTest = p -> p.getColor() == ProductColor.RED;
    Predicate<Product> productSizeTest = p -> p.getSize() == ProductSize.XXL;
    System.out.println("Red Products " + pf.filter(Product.getProducts(), productColorTest));
    System.out.println("XXL Products " + pf.filter(Product.getProducts(), productSizeTest));
    // we could do this with pf.filter too, but chose to inline the method
    System.out.println("Red XXL Products " +
        Product.getProducts().stream().filter(productColorTest.and(productSizeTest)).toList());
  }
}
