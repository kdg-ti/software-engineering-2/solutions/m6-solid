package exercises.ex5.solution_2_solid_final;

import exercises.ex5.model.Product;

public interface ProductTest {
    public boolean match(Product product);
}

