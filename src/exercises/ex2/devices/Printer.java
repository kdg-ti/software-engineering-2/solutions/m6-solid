package exercises.ex2.devices;

import exercises.ex2.core.Writer;

public class Printer implements Writer {

  @Override
    public void write(char i) {
	    System.out.println(i);
    }
}
