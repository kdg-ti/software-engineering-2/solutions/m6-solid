package exercises.ex5.model;


import java.util.List;

import static exercises.ex5.model.ProductColor.*;
import static exercises.ex5.model.ProductSize.*;

public class Product {
    private ProductColor color;
    private ProductSize size;
    public ProductColor getColor() {
        return color;
    }
    public ProductSize getSize() {
        return size;
    }



    public Product(ProductColor color, ProductSize size) {
        this.color = color;
        this.size = size;
    }

    public Product() {
        this(GRAY,ProductSize.M);
    }
    @Override
    public String toString() {
        return "Product{" +
          "color=" + color +
          ", size=" + size +
          '}';
    }

    public static List<Product> getProducts(){
        return  List.of(
          new Product(GRAY, S),
          new Product(GRAY,XXL),
          new Product(RED, M),
          new Product(RED, XXL),
          new Product(CHARTREUSE,M),
          new Product(CHARTREUSE,S),
          new Product(CHARTREUSE,XXL)

        );
    }
}
