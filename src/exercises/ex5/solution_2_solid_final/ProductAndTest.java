package exercises.ex5.solution_2_solid_final;

import exercises.ex5.model.Product;

public class ProductAndTest implements ProductTest {
	private ProductTest[] tests;

	public ProductAndTest(ProductTest... tests) {
		this.tests = tests;
	}

	@Override
	public boolean match(Product product) {
		for (ProductTest test:tests){
			if(!test.match(product)){
				return false;
			}
		}
		return true;
	}
}
