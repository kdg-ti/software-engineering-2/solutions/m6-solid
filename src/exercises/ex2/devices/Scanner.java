package exercises.ex2.devices;

import exercises.ex2.core.Reader;

public class Scanner implements Reader {

    @Override
    public char read() {
        return '\u0000';
    }

    @Override
    public boolean hasNext() {
        return false;
    }
}
