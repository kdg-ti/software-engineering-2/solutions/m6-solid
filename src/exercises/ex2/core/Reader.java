package exercises.ex2.core;

public interface Reader {
    public char read();
    public boolean hasNext();
}
