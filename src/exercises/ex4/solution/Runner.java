package exercises.ex4.solution;

public class Runner {

		// SRP: this class has the single responsibility to start the program
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	// creating an object and calling an instance method for OO design
	            // static methods as in the original solution are bad for Open/Closed compatible design
                Application b = new Application();
                b.createAndShowGUI();
            }
        });
    }
}
