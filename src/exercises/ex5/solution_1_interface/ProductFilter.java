package exercises.ex5.solution_1_interface;

import exercises.ex5.model.Product;

import java.util.List;

public interface ProductFilter{
    public List<Product> filter(List<Product> products);
}

