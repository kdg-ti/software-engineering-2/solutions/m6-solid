package exercises.ex4.solution;



import javax.swing.*;
import java.awt.*;

// This class contains the bulk of the application.
// Why was the original class from the Java Tutorial so badly designed?
// The answer is in the comments of that class: to allow the example to be downloadable as a singel java file
public class Application extends JPanel {
	JButton button;

	public Application() {
		super(new BorderLayout());
		button = new JButton("Click Me");
		button.setPreferredSize(new Dimension(200, 80));
		add(button, BorderLayout.CENTER);
		button.addActionListener(new Beeper());
	}


	/**
	 * Create the GUI and show it.  For thread safety,
	 * this method should be invoked from the
	 * event-dispatching thread.
	 */
	public void createAndShowGUI() {
		//Create and set up the window.
		JFrame frame = new JFrame("Beeper");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Create and set up the content pane.
		JComponent newContentPane = new Application();
		newContentPane.setOpaque(true); //content panes must be opaque
		frame.setContentPane(newContentPane);

		//Display the window.
		frame.pack();
		frame.setVisible(true);
	}
}