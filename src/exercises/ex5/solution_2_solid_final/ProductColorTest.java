package exercises.ex5.solution_2_solid_final;

import exercises.ex5.model.Product;
import exercises.ex5.model.ProductColor;

public class ProductColorTest implements ProductTest {
	private ProductColor productColor;

	public ProductColorTest(ProductColor productColor) {
		this.productColor = productColor;
	}

	@Override
	public boolean match(Product product) {
		return product.getColor() == productColor;
	}
}
