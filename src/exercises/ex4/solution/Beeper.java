package exercises.ex4.solution;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// SRP: this class has tho single repsonsibility to beep
// It only implements an interface, but does not extend anything:
// it can still extend a class that specialises the beeping behaviour
// It should not extend a class that has the responsibility to show a UI
public class Beeper
	implements ActionListener {
	public void actionPerformed(ActionEvent e) {
		Toolkit.getDefaultToolkit().beep();
	}
}