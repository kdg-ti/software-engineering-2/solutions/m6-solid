package exercises.ex5.solution_0_start;

import exercises.ex5.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jan de Rijke.
 */
public class ProductFilter {
	public List<Product> filterByColor(List<Product> products, ProductColor productColor) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (product.getColor() == productColor) {filteredProducts.add(product);}
		}
		return filteredProducts;
	}

	public List<Product> filterBySize(List<Product> products, ProductSize productSize) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (product.getSize() == productSize) {filteredProducts.add(product);}
		}
		return filteredProducts;
	}

	public List<Product> filterByColorAndSize(
		List<Product> products,
		ProductColor productColor,
		ProductSize productSize) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product : products) {
			if (product.getSize() == productSize && product.getColor() == productColor) {
				filteredProducts.add(product);
			}
		}
		return filteredProducts;
	}


}