package exercises.ex2;

import exercises.ex2.devices.Keyboard;
import exercises.ex2.devices.Printer;
import exercises.ex2.core.*;


public class Runner {

    public static void main(String[] args) {
        Writer w= new Printer();
        Reader r = new Keyboard("I'm");
        Copier d = new Copier();
        d.copy(r,w);
    }
}
