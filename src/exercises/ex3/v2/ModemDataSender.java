package exercises.ex3.v2;

public interface ModemDataSender
{
    public void send(char c);
}
