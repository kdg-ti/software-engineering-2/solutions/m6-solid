package exercises.ex5.solution_0_start;

import exercises.ex5.model.Product;


import static exercises.ex5.model.ProductColor.GRAY;
import static exercises.ex5.model.ProductSize.S;


public class Runner {

    public static void main(String[] args) {

        ProductFilter pf = new ProductFilter();
        System.out.println("Gray products: "+pf.filterByColor(Product.getProducts(), GRAY));
        System.out.println("S products: "+pf.filterBySize(Product.getProducts(), S));
        System.out.println("Gray S products: "+pf.filterByColorAndSize(Product.getProducts(), GRAY,S));
    }
}
