package exercises.ex5.solution_1_interface;

import exercises.ex5.model.Product;
import exercises.ex5.model.ProductColor;

import java.util.*;

public class ProductColorFilter implements ProductFilter
{
    private ProductColor productColor;

    public ProductColorFilter(ProductColor productColor){
        this.productColor = productColor;
    }


	@Override
	public List<Product> filter(List<Product> products) {
		List<Product> filteredProducts = new ArrayList<>();
		for (Product product:products)
		{
			if (product.getColor() == productColor)
			{filteredProducts.add(product);}
		}
		return filteredProducts;	}
}
